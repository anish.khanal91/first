import React, { Component } from 'react';

class DisplayItem extends Component {
    render() {
        return (
            <tr>
                <td>{this.props.itemElements.ID}</td>
                <td>{this.props.itemElements.Name}</td>
                <td>{this.props.itemElements.Email}</td>
                <td>{this.props.itemElements.Delete}</td>
                <td><span style={{color: 'red '}} onClick={() => this.props.deleteItem(this.props.itemElements.ID)}>Delete</span></td>
            </tr>
        );
    }
}

export default DisplayItem ;