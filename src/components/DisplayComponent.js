import { Component } from 'react';
import Displayitem from './DisplayItem';

class DisplayComponent extends Component {
    render() {
        return (
            <div>
                <table>
                    <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
            
                        
                    </thead>

                    <tbody>
                    {this.props.myArray.map((item) => {
                        return <Displayitem deleteItem={this.props.handleDelete} itemElements={item} />
                    })} 
                    </tbody>
                </table>

                <p></p>
            </div>
        );
    }
}

export default DisplayComponent;