import React, { Component } from 'react' ;
import DisplayComponent from './DisplayComponent' ;

class MapComponent extends Component {

    state = {
        myArray: [
            {
                ID:1,
                Name: 'Anish',
                Email:"Anish@gmail.com"
            },
            {
                ID: 2,
                Name: 'Amrit',
                Email: "Amrit@gmail.com"
            },
            {
                ID: 3,
                Name: 'Anisha',
                Email: "Anisha@gmail.com"
            },
            {
                ID: 4,
                Name: 'Tara',
                Email: "Tara@gmail.com"
            },
        ]
    }

    handleDelete = (id) => {
        this.setState({ myArray: this.state.myArray.filter(function( obj ) {
                return obj.ID !== id;
            })
        });

        

        // logic to delete item from myArray based on id given
    }

    showData() {
        return <DisplayComponent handleDelete={this.handleDelete} myArray={this.state.myArray}
        />
    }
    render() {
        return (
            <div>
                <h1>Map Component</h1>
                {this.showData()}
            </div>
        );
    }
}

export default MapComponent;